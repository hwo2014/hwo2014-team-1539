## Todo ##

1. ** http://www.reddit.com/r/HWO/comments/239jir/about_mono_compilers/ ** xbuild is up in the air still and gcms DOES mean we're limited to .net 2.0

2. Get building with VS (ideally with msbuild)
3. Use the output of this to set up all the wrappers with xbuild
4. .. tbc

## Changelog ##

* Ed / Rich (21/April/2014) 
 ** tidied up the source a lot
 ** got the build/run to work with xbuild
* Ed (18/April/2014) actually the .sln works with xbuild already
* Ed (18/April/2014) rm'd other languages fluff
