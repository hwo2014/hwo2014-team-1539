﻿using System.Collections.Generic;

namespace hwo2014.Model.Messages
{
    public class PositionData
    {
        public CarId id { get; set; }
        public float angle { get; set; }
        public IDictionary<string, object> piecePosition { get; set; }

    }
}
