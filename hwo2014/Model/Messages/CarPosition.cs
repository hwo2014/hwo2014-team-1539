﻿namespace hwo2014.Model.Messages
{
    public class CarPosition
    {

        public string msgType { get; set; }
        public PositionData[] data { get; set; }
        public string gameId { get; set; }
        public int gameTick { get; set; }
    }
}
