﻿namespace hwo2014.Model.Messages
{
    public class Ping : SendMsg
    {
        protected override string MsgType()
        {
            return "ping";
        }
    }

}
