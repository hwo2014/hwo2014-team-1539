﻿using System;

namespace hwo2014.Model.Messages
{
    public class MsgWrapper
    {
        public string msgType { get; private set; }
        public Object data { get; private set; }

        public MsgWrapper(string msgType, Object data)
        {
            this.msgType = msgType;
            this.data = data;
        }
    }
}
