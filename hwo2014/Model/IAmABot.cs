﻿using hwo2014.Model.Messages;

namespace hwo2014.Model
{
    public interface IAmABot
    {
        SendMsg CarPositions(CarPosition carPosition);
        SendMsg GameEnd();
        SendMsg GameInit(GameInit gameInit);
        SendMsg GameStart();
        SendMsg Join();
        string Key { get; }
        string Name { get; }
    }
}
