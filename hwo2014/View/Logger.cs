﻿using System;
using System.Diagnostics;

namespace hwo2014.View
{
    public class Logger
    {

        private static string getCallerName()
        {
            return new StackTrace().GetFrame(2).GetMethod().Name;
        }

        private static string FatalError(Exception msg, string caller)
        {
            if (msg == null)
                throw new ArgumentNullException();

            return Info(" [ FATAL ERROR ] " + msg.ToString(), caller==null?getCallerName():caller);
        }

        public static string FatalError(Exception msg)
        {
            return FatalError(msg, getCallerName());
        }

        private static string Info(string msg, string caller)
        {
            if (msg == null)
                throw new ArgumentNullException();

            var ts = DateTime.Now;

            var res = String.Format("{0} {1}: {2}", ts.ToString("dd/MM/yyyy HH:mm.ss"), caller==null?getCallerName():caller, msg);
            
            Console.WriteLine(res);

            return res;
        }


        public static string Info(string msg)
        {
            return Info(msg, getCallerName());
        }


    }
}
