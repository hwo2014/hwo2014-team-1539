﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using hwo2014.Model;
using hwo2014.Model.Messages;
using hwo2014.View;
using Newtonsoft.Json;

namespace hwo2014.Controller
{
    public class BotRunner
    {
        private StreamWriter writer;
        private StreamReader reader;

        public BotRunner(StreamReader reader, StreamWriter writer)
        {
            this.reader = reader;
            this.writer = writer;
        }

        private void WaitTillDataAvailable(int timeout)
        {
            var i = 0;
            while (!((NetworkStream)reader.BaseStream).DataAvailable && i < timeout)
            {
                Logger.Info("Sleeping 500ms");
                Thread.Sleep(500);
                i += 500;
                Logger.Info(String.Format( "Data Available: {0}", ((NetworkStream)reader.BaseStream).DataAvailable ) );
            }
        }

        public void Run(IAmABot bot)
        {
            send(new Join(bot.Name, bot.Key));

            string line;

            //Logger.Info("Waiting for data..");
            //WaitTillDataAvailable(5000);
            //Logger.Info("finished waiting for data");

            while (reader.EndOfStream != true)
            {
                line = reader.ReadLine();
 
                var msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
                Logger.Info(string.Format("from server: {0}", msg.msgType));

                switch (msg.msgType)
                {
                    case "carPositions":
                        send(bot.CarPositions(JsonConvert.DeserializeObject<CarPosition>(line)));
                        break;
                    case "yourCar":
                        Logger.Info("server: " + line);
                        break;
                    case "join":
                        Logger.Info("Joined");
                        send(bot.Join());
                        break;
                    case "gameInit":
                        Logger.Info("Race init");
                        send(bot.GameInit(MapToGameInit(msg.data)));
                        break;
                    case "gameEnd":
                        Logger.Info("Race ended");
                        send(bot.GameEnd());
                        break;
                    case "gameStart":
                        Logger.Info("Race starts");
                        send(bot.GameStart());
                        break;
                    case "crash":
                        Logger.Info("!!!!--- C R A S H ---!!!");
                        send(new Ping());
                        break;
                    default:
                        send(new Ping());
                        Logger.Info("unkown message recieved!?");
                        break;
                }
            }

            Logger.Info("finished");
        }

        private GameInit MapToGameInit(object p)
        {
            return null;
        }

        private void send(SendMsg msg)
        {
            Logger.Info(msg.ToJson());
            writer.WriteLine(msg.ToJson());
            writer.Flush();
        }

    }
}
