﻿using hwo2014.Model;
using hwo2014.Model.Messages;

namespace hwo2014.Controller
{
    /// <summary>
    /// Relies on keeping throttle constantly at 0.5.
    /// WORKS! but slow
    /// </summary>
    public class SlowBot : IAmABot
    {
        private string botName;
        private string botKey;

        public SlowBot(string botName, string botKey)
        {
            this.botName = botName;
            this.botKey = botKey;
        }

        public string Name
        {
            get { return botName; }
        }

        public string Key
        {
            get { return botKey; }
        }

        public SendMsg GameInit(GameInit gameInit)
        {
            return new Ping();
        }

        public SendMsg CarPositions(CarPosition carPosition)
        {
            return new Throttle(0.5);
        }

        public SendMsg Join()
        {
            return new Ping();
        }

        public SendMsg GameEnd()
        {
            return new Ping();
        }

        public SendMsg GameStart()
        {
            return new Ping();
        }
    }
}
