﻿using System;
using hwo2014.Model;
using hwo2014.Model.Messages;
using hwo2014.View;

namespace hwo2014.Controller
{
    /// <summary>
    /// Relies purly on controling slipage by reducing throtle based on amount of slippage.
    /// Doesnot work!
    /// </summary>
    public class CrazyBot : IAmABot
    {
        private string botName;
        private string botKey;

        public CrazyBot(string botName, string botKey)
        {
            this.botName = botName;
            this.botKey = botKey;
        }

        public string Name
        {
            get { return botName; }
        }

        public string Key
        {
            get { return botKey; }
        }

        public SendMsg GameInit(GameInit gameInit)
        {
            return new Ping();
        }

        public SendMsg CarPositions(CarPosition carPosition)
        {
            var maxSpeed = 0.8f;
            var angle = carPosition.data[0].angle;
            Logger.Info(String.Format( "angle: {0}", angle ));

            angle = Math.Abs(angle);

            var angleFactor = angle / 5.0f;
            var damping = Math.Min(angleFactor, 1.0f);
            Logger.Info(String.Format( "damping: {0}", damping ));
            var throttle = (1.0f - damping) * maxSpeed;

            Logger.Info(String.Format( "final throttle: {0}", throttle ));

            return new Throttle(throttle);
        }

        public SendMsg Join()
        {
            return new Ping();
        }

        public SendMsg GameEnd()
        {
            return new Ping();
        }

        public SendMsg GameStart()
        {
            return new Ping();
        }


    }
}
