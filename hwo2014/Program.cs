using System;
using System.IO;
using System.Net.Sockets;
using hwo2014.Controller;
using hwo2014.View;

namespace hwo2014
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                if (args.Length < 4)
                {
                    Console.WriteLine("Please pass four command line arguments: host port botName botKey.");
                    return;
                }

                var host = args[0];
                var port = int.Parse(args[1]);
                var botName = args[2];
                var botKey = args[3];

                Logger.Info("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

                using (var client = new TcpClient(host, port))
                {
                    if (client.Connected)
                    {
                        Logger.Info("Connected!");
                    }
                    using (var stream = client.GetStream())
                    using (var reader = new StreamReader(stream))
                    using (var writer = new StreamWriter(stream))
                    {
                        writer.AutoFlush = true;
                        Logger.Info(String.Format("Can Read: {0}", stream.CanRead ));
                        Logger.Info(String.Format("Data Available: {0}", stream.DataAvailable));                        

                        var botRunner = new BotRunner(reader, writer);

                        // RP: 19/04/2014: Change the type of the IAmABot 
                        // implementor passed to botRunner to change strategy.
                        botRunner.Run(new CrazyBot(botName, botKey));

                        reader.Close();
                        writer.Close();
                        stream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.FatalError(ex);
            }
        }
    }
}