﻿using hwo2014.Model.Messages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace hwo2014.Tests
{
    [TestClass]
    public class BotTests
    {
        [TestMethod]
        public void Test()
        {
            var result = JsonConvert.DeserializeObject<CarPosition>("{\"msgType\":\"carPositions\",\"data\":[{\"id\":{\"name\":\"speedDemon\",\"color\":\"red\"},\"angle\":0.0,\"piecePosition\":{\"pieceIndex\":0,\"inPieceDistance\":0.0,\"lane\":{\"startLaneIndex\":0,\"endLaneIndex\":0},\"lap\":0}}],\"gameId\":\"55f861f6-4048-4597-9372-72113aa535f9\"}");

            Assert.AreEqual(result.msgType, "carPositions");
            Assert.AreEqual(result.data[0].id.name, "speedDemon");
            Assert.AreEqual(result.data[0].id.color, "red");
            Assert.AreEqual(result.data[0].angle, 0.0f);
        }
    }
}
