﻿using System;
using hwo2014.View;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace hwo2014.Tests
{
    [TestClass]
    public class LoggerTest
    {

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void nullLogsNotOk()
        {
            Logger.Info(null);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void nullFatalsNotOk()
        {
            Logger.FatalError(null);
        }

        class HelloException : Exception
        {
            public override string ToString() { return "HELLOWORLD"; }
        }    

        [TestMethod]
        public void writeFatalTest()
        {             
            var res = Logger.FatalError(new HelloException());
            var dt = DateTime.Now.ToString("dd/MM/yyyy HH:mm.ss");
            Assert.AreEqual(dt + " " + "writeFatalTest: " + " [ FATAL ERROR ] HELLOWORLD", res);
        }


        [TestMethod]
        public void writeALogTest()
        {
            var res = Logger.Info("hello world");
            var dt = DateTime.Now.ToString("dd/MM/yyyy HH:mm.ss");
            Assert.AreEqual(dt + " " + "writeALogTest: " + "hello world", res);
        }

    }
}
